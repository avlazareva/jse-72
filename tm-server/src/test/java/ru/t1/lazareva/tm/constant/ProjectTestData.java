package ru.t1.lazareva.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.dto.model.ProjectDto;
import ru.t1.lazareva.tm.dto.model.UserDto;
import ru.t1.lazareva.tm.enumerated.Status;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@UtilityClass
public final class ProjectTestData {

    @Nullable
    public final static String USER_1_LOGIN = "test";

    @Nullable
    public final static String USER_1_PASSWORD = "test";

    @Nullable
    public final static String USER_1_EMAIL = "test@test.ru";

    @Nullable
    public final static String USER_2_LOGIN = "admin";

    @Nullable
    public final static String USER_2_PASSWORD = "admin";

    @Nullable
    public final static String USER_2_EMAIL = "admin@admin.ru";

    @NotNull
    public final static UserDto USER_1 = new UserDto(USER_1_LOGIN, USER_1_PASSWORD, USER_1_EMAIL);
    @NotNull
    public final static ProjectDto USER_PROJECT1 = new ProjectDto(USER_1, "project1", "123", Status.NOT_STARTED);
    @NotNull
    public final static UserDto USER_2 = new UserDto(USER_2_LOGIN, USER_2_PASSWORD, USER_2_EMAIL);
    @NotNull
    public final static ProjectDto USER_PROJECT2 = new ProjectDto(USER_2, "project2");
    @NotNull
    public final static List<ProjectDto> PROJECT_LIST = Arrays.asList(USER_PROJECT1, USER_PROJECT2);
    @Nullable
    public final static ProjectDto NULL_PROJECT = null;
    @NotNull
    public final static String NON_EXISTING_PROJECT_ID = UUID.randomUUID().toString();

}