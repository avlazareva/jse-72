package ru.t1.lazareva.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.lazareva.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @NotNull
    @Value("#{environment['application.name']}")
    public String applicationName;

    @NotNull
    @Value("#{environment['application.config']}")
    public String applicationConfig;

    @NotNull
    @Value("#{environment['application.log']}")
    public String applicationLogs;

    @NotNull
    @Value("#{environment['buildNumber']}")
    public String applicationVersion;

    @NotNull
    @Value("#{environment['email']}")
    public String authorEmail;

    @NotNull
    @Value("#{environment['developer']}")
    public String authorName;

    @NotNull
    @Value("#{environment['gitBranch']}")
    public String gitBranch;

    @NotNull
    @Value("#{environment['gitCommitId']}")
    public String gitCommitId;

    @NotNull
    @Value("#{environment['gitCommitterName']}")
    public String gitCommitterName;

    @NotNull
    @Value("#{environment['gitCommitterEmail']}")
    public String gitCommitterEmail;

    @NotNull
    @Value("#{environment['gitCommitMessage']}")
    public String gitCommitMessage;

    @NotNull
    @Value("#{environment['gitCommitTime']}")
    public String gitCommitTime;

    @NotNull
    @Value("#{environment['password.iteration']}")
    public Integer passwordIteration;

    @NotNull
    @Value("#{environment['password.secret']}")
    public String passwordSecret;

    @NotNull
    @Value("#{environment['server.port']}")
    public Integer serverPort;

    @NotNull
    @Value("#{environment['server.host']}")
    public String serverHost;

    @NotNull
    @Value("#{environment['session.timeout']}")
    public Integer sessionTimeout;

    @NotNull
    @Value("#{environment['session.key']}")
    public String sessionKey;

    @NotNull
    @Value("#{environment['database.username']}")
    public String dBUser;

    @NotNull
    @Value("#{environment['database.password']}")
    public String dBPassword;

    @NotNull
    @Value("#{environment['database.url']}")
    public String DBUrl;

    @NotNull
    @Value("#{environment['database.driver']}")
    public String DBDriver;

    @NotNull
    @Value("#{environment['database.dialect']}")
    public String DBDialect;

    @NotNull
    @Value("#{environment['database.hbm2ddl_auto']}")
    public String DBHbm2DDL;

    @NotNull
    @Value("#{environment['database.showSql']}")
    public String DBShowSQL;

    @NotNull
    @Value("#{environment['database.second_lvl_cache']}")
    public String DBSecondLvlCache;

    @NotNull
    @Value("#{environment['database.factory_class']}")
    public String DBFactoryClass;

    @NotNull
    @Value("#{environment['database.useQueryCache']}")
    public String DBQueryCache;

    @NotNull
    @Value("#{environment['database.useMinPuts']}")
    public String DBMinimalPuts;

    @NotNull
    @Value("#{environment['database.regionPrefix']}")
    public String DBCacheRegionPrefix;

    @NotNull
    @Value("#{environment['database.cacheRegionClass']}")
    public String DBCacheRegion;

    @NotNull
    @Value("#{environment['database.configFilePath']}")
    public String DBCacheProvider;

    @NotNull
    @Value("#{environment['database.schema']}")
    public String DBSchema;

    @NotNull
    @Value("#{environment['database.cache']}")
    public String DBCache;

}
