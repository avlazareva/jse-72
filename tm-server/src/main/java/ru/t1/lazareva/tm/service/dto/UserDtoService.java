package ru.t1.lazareva.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.lazareva.tm.api.service.dto.IUserDtoService;
import ru.t1.lazareva.tm.dto.model.UserDto;
import ru.t1.lazareva.tm.enumerated.Role;
import ru.t1.lazareva.tm.exception.entity.EntityNotFoundException;
import ru.t1.lazareva.tm.exception.entity.UserNotFoundException;
import ru.t1.lazareva.tm.exception.field.EmailEmptyException;
import ru.t1.lazareva.tm.exception.field.IdEmptyException;
import ru.t1.lazareva.tm.exception.field.LoginEmptyException;
import ru.t1.lazareva.tm.exception.field.PasswordEmptyException;
import ru.t1.lazareva.tm.exception.user.ExistsEmailException;
import ru.t1.lazareva.tm.exception.user.ExistsLoginException;
import ru.t1.lazareva.tm.exception.user.RoleEmptyException;
import ru.t1.lazareva.tm.repository.dto.UserDtoRepository;
import ru.t1.lazareva.tm.service.PropertyService;
import ru.t1.lazareva.tm.util.HashUtil;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class UserDtoService extends AbstractDtoService<UserDto, UserDtoRepository> implements IUserDtoService {

    @NotNull
    @Autowired
    private UserDtoRepository repository;

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDto create(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull UserDto user = new UserDto();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        repository.save(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDto create(@Nullable final String login, @Nullable final String password, @Nullable final String email) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExists(email)) throw new ExistsEmailException();
        @NotNull UserDto user = new UserDto();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        repository.save(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDto create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull UserDto user = new UserDto();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        repository.save(user);
        return user;
    }

    @Nullable
    @Override
    public UserDto findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable UserDto resultModel;
        resultModel = repository.findByLogin(login);
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;
    }

    @Nullable
    @Override
    public UserDto findByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable UserDto resultModel;
        resultModel = repository.findByEmail(email);
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;
    }

    @Override
    public Boolean isLoginExists(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return false;
        return (repository.findByLogin(login) != null);
    }

    @Override
    public Boolean isEmailExists(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) return false;
        return (repository.findByEmail(email) != null);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void lockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDto user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        repository.save(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByLogin(@Nullable final String login) throws Exception {
        @Nullable final UserDto user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        repository.delete(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public UserDto setPassword(@Nullable final String id, @Nullable final String password) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDto user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        @Nullable UserDto resultModel;
        resultModel = repository.save(user);
        return resultModel;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDto user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        repository.save(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public UserDto update(@Nullable final String id, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDto user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName((firstName == null) ? "" : firstName);
        user.setLastName((lastName == null) ? "" : lastName);
        user.setMiddleName((middleName == null) ? "" : middleName);
        @Nullable UserDto resultModel;
        resultModel = repository.save(user);
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;
    }

}
