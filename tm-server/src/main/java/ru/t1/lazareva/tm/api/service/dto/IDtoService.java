package ru.t1.lazareva.tm.api.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.lazareva.tm.dto.model.AbstractModelDto;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IDtoService<M extends AbstractModelDto> {

    @NotNull
    M add(@Nullable M model);

    @NotNull Collection<M> add(@Nullable Collection<M> models);

    void clear() throws Exception;

    boolean existsById(@Nullable String id) throws Exception;

    @Nullable List<M> findAll();

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator) throws Exception;

    M findOneById(@Nullable String id);

    int getSize() throws Exception;

    @SneakyThrows
    @Transactional
    void remove(M model);

    void removeById(@Nullable String id) throws Exception;

    @SneakyThrows
    @Transactional
    M update(M model) throws Exception;
}

