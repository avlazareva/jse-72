package ru.t1.lazareva.tm.service.model;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.lazareva.tm.api.service.model.IProjectService;
import ru.t1.lazareva.tm.enumerated.Status;
import ru.t1.lazareva.tm.exception.entity.ProjectNotFoundException;
import ru.t1.lazareva.tm.exception.field.*;
import ru.t1.lazareva.tm.model.Project;
import ru.t1.lazareva.tm.repository.model.ProjectRepository;

@Service
@NoArgsConstructor
public final class ProjectService extends AbstractUserOwnedService<Project, ProjectRepository> implements IProjectService {

    @NotNull
    @Autowired
    private ProjectRepository repository;

    @Override
    @SneakyThrows
    @Transactional
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        if (status != null)
            project.setStatus(status);
        repository.save(project);
        return project;
    }

    @Override
    @SneakyThrows
    @Transactional
    public Project changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null) throw new IndexIncorrectException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        if (status != null)
            project.setStatus(status);
        repository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project create(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Project project = new Project();
        project.setUser(userService.findOneById(userId));
        project.setName(name);
        repository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Project project = new Project();
        project.setUser(userService.findOneById(userId));
        project.setName(name);
        project.setDescription(description);
        repository.save(project);
        return project;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setUser(userService.findOneById(userId));
        project.setName(name);
        project.setDescription(description);
        repository.save(project);
    }

}
