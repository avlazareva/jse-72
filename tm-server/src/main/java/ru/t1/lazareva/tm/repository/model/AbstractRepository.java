package ru.t1.lazareva.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.lazareva.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
public interface AbstractRepository<M extends AbstractModel> extends JpaRepository<M, String> {

    boolean existsById(@NotNull final String id);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@NotNull final Comparator<M> comparator);

    @Nullable
    M findOneById(@NotNull final String id);

    @Nullable
    M findOneByIndex(@NotNull final Integer index);

    int getSize();

    void remove(@NotNull final M model);

    void removeById(@NotNull final String id);

    @Nullable
    M update(@NotNull final M model);

}