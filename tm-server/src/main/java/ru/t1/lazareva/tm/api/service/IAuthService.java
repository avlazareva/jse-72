package ru.t1.lazareva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.dto.model.SessionDto;
import ru.t1.lazareva.tm.dto.model.UserDto;

public interface IAuthService {

    @NotNull
    String login(@Nullable String login, @Nullable String password) throws Exception;

    @NotNull
    UserDto check(@Nullable String login, @Nullable String password) throws Exception;

    @NotNull
    SessionDto validateToken(@Nullable String token) throws Exception;

    void invalidate(@Nullable final SessionDto session) throws Exception;

    @NotNull
    UserDto registry(@NotNull String login, @NotNull String password, @NotNull String email) throws Exception;

}