package ru.t1.lazareva.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.lazareva.tm.dto.model.TaskDto;
import ru.t1.lazareva.tm.dto.request.TaskListRequest;
import ru.t1.lazareva.tm.enumerated.Sort;
import ru.t1.lazareva.tm.event.ConsoleEvent;
import ru.t1.lazareva.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public final class TaskListListener extends AbstractTaskListener {

    @NotNull
    private static final String NAME = "task-list";

    @NotNull
    private static final String DESCRIPTION = "Show task list.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@taskListListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @NotNull final Sort sort = Sort.toSort(sortType);
        @NotNull final TaskListRequest request = new TaskListRequest(getToken());
        request.setSortType(sort);
        @Nullable final List<TaskDto> tasks = taskEndpoint.listTask(request).getTasks();
        if (tasks != null) renderTasks(tasks);
    }

}