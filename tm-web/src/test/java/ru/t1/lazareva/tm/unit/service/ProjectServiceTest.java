package ru.t1.lazareva.tm.unit.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.lazareva.tm.api.service.IProjectDtoService;
import ru.t1.lazareva.tm.config.ApplicationConfiguration;
import ru.t1.lazareva.tm.dto.ProjectDTO;
import ru.t1.lazareva.tm.exception.IdEmptyException;
import ru.t1.lazareva.tm.exception.UserIdEmptyException;
import ru.t1.lazareva.tm.marker.UnitCategory;
import ru.t1.lazareva.tm.util.UserUtil;

import java.util.Arrays;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
@Category(UnitCategory.class)
public class ProjectServiceTest {

    @NotNull
    private final ProjectDTO project1 = new ProjectDTO("Test Project 1");
    @NotNull
    private final ProjectDTO project2 = new ProjectDTO("Test Project 2");
    @NotNull
    private final ProjectDTO project3 = new ProjectDTO("Test Project 3");
    @NotNull
    private final ProjectDTO project4 = new ProjectDTO("Test Project 4");
    @NotNull
    @Autowired
    private IProjectDtoService projectDtoService;
    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void initTest() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("admin", "admin");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        project1.setUserId(UserUtil.getUserId());
        project2.setUserId(UserUtil.getUserId());
        project3.setUserId(UserUtil.getUserId());
        projectDtoService.save(UserUtil.getUserId(), project1);
        projectDtoService.save(UserUtil.getUserId(), project2);
    }

    @After
    public void clear() {
        projectDtoService.removeAll(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    public void saveTest() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.save(null, project1));
        Assert.assertThrows(IdEmptyException.class, () -> projectDtoService.save(UserUtil.getUserId(), null));
        Assert.assertEquals(2, projectDtoService.findAll(UserUtil.getUserId()).size());
        projectDtoService.save(UserUtil.getUserId(), project3);
        Assert.assertEquals(3, projectDtoService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void saveAll() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.saveAll(null, Arrays.asList(project1, project2)));
        Assert.assertThrows(IdEmptyException.class, () -> projectDtoService.saveAll(UserUtil.getUserId(), null));
        Assert.assertEquals(2, projectDtoService.findAll(UserUtil.getUserId()).size());
        projectDtoService.saveAll(UserUtil.getUserId(), Arrays.asList(project3, project4));
        Assert.assertEquals(4, projectDtoService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void removeAllByUserIdTest() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.removeAll(null));
        Assert.assertEquals(2, projectDtoService.findAll(UserUtil.getUserId()).size());
        projectDtoService.removeAll(UserUtil.getUserId());
        Assert.assertEquals(0, projectDtoService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void removeAllTest() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.removeAll(null));
        Assert.assertEquals(2, projectDtoService.findAll(UserUtil.getUserId()).size());
        projectDtoService.removeAll(UserUtil.getUserId());
        Assert.assertEquals(0, projectDtoService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void removeOneByIdTest() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.removeOneById(null, project1.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> projectDtoService.removeOneById(UserUtil.getUserId(), null));
        Assert.assertThrows(IdEmptyException.class, () -> projectDtoService.removeOneById(UserUtil.getUserId(), ""));
        Assert.assertEquals(2, projectDtoService.findAll(UserUtil.getUserId()).size());
        projectDtoService.removeOneById(UserUtil.getUserId(), project1.getId());
        Assert.assertEquals(1, projectDtoService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void findAllTest() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.findAll(null));
        Assert.assertEquals(2, projectDtoService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void findOneByIdTest() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.findOneById(null, project1.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> projectDtoService.findOneById(UserUtil.getUserId(), null));
        Assert.assertThrows(IdEmptyException.class, () -> projectDtoService.findOneById(UserUtil.getUserId(), ""));
        Assert.assertEquals(project1.getName(), projectDtoService.findOneById(UserUtil.getUserId(), project1.getId()).getName());
    }

}