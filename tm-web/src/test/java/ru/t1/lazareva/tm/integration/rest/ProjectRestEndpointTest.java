package ru.t1.lazareva.tm.integration.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.t1.lazareva.tm.dto.CredentialsDto;
import ru.t1.lazareva.tm.dto.ProjectDTO;
import ru.t1.lazareva.tm.dto.Result;
import ru.t1.lazareva.tm.marker.IntegrationCategory;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.List;

@Category(IntegrationCategory.class)
public class ProjectRestEndpointTest {

    @NotNull
    private static final String PROJECT_URL = "http://localhost:8080/api/project";
    @NotNull
    private static final String PROJECTS_URL = "http://localhost:8080/api/projects";
    @NotNull
    private static final HttpHeaders HEADER = new HttpHeaders();
    @Nullable
    private static String sessionId;
    @NotNull
    private final ProjectDTO project1 = new ProjectDTO("Test Project 1");
    @NotNull
    private final ProjectDTO project2 = new ProjectDTO("Test Project 2");
    @NotNull
    private final ProjectDTO project3 = new ProjectDTO("Test Project 3");

    @BeforeClass
    public static void beforeClass() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/api/auth/login";
        @NotNull CredentialsDto credentials = new CredentialsDto("admin", "admin");
        @NotNull final ResponseEntity<Result> response = restTemplate.postForEntity(url, new HttpEntity<CredentialsDto>(credentials, HEADER), Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        HttpHeaders headersResponse = response.getHeaders();
        List<HttpCookie> cookies = HttpCookie.parse(headersResponse.getFirst(HttpHeaders.SET_COOKIE));
        sessionId = cookies.stream()
                .filter(item -> "JSESSIONID".equals(item.getName()))
                .findFirst()
                .get()
                .getValue();
        Assert.assertNotNull(sessionId);
        HEADER.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
        HEADER.setContentType(MediaType.APPLICATION_JSON);
    }

    private static ResponseEntity<List> sendRequestList(@NotNull final String url, HttpMethod method, @NotNull final HttpEntity<List> httpEntity) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, List.class);
    }

    private static ResponseEntity<ProjectDTO> sendRequest(@NotNull final String url, HttpMethod method, @NotNull final HttpEntity<ProjectDTO> httpEntity) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, ProjectDTO.class);
    }

    @AfterClass
    public static void logout() {
        @NotNull final String url = "http://localhost:8080/api/auth/logout";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(HEADER));
    }

    @Before
    public void init() {
        sendRequestList(PROJECTS_URL, HttpMethod.POST, new HttpEntity<>(Arrays.asList(project1, project2), HEADER));
    }

    @After
    public void clear() {
        sendRequestList(PROJECTS_URL, HttpMethod.DELETE, new HttpEntity<>(sendRequestList(PROJECTS_URL, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody(), HEADER));
    }

    @Test
    public void getTest() {
        Assert.assertEquals(project1.getName(), sendRequest(PROJECT_URL + "/" + project1.getId(), HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().getName());
    }

    @Test
    public void postTest() {
        Assert.assertEquals(2, sendRequestList(PROJECTS_URL, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
        sendRequest(PROJECT_URL, HttpMethod.POST, new HttpEntity<>(project3, HEADER));
        Assert.assertEquals(3, sendRequestList(PROJECTS_URL, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
    }

    @Test
    public void putTest() {
        Assert.assertEquals(2, sendRequestList(PROJECTS_URL, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
        project1.setName("Name 1");
        sendRequest(PROJECT_URL, HttpMethod.POST, new HttpEntity<>(project1, HEADER));
        Assert.assertEquals(2, sendRequestList(PROJECTS_URL, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
        Assert.assertEquals("Name 1", sendRequest(PROJECT_URL + "/" + project1.getId(), HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().getName());
    }

    @Test
    public void deleteTest() {
        Assert.assertEquals(2, sendRequestList(PROJECTS_URL, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
        sendRequest(PROJECT_URL + "/" + project1.getId(), HttpMethod.DELETE, new HttpEntity<>(project1, HEADER));
        Assert.assertEquals(1, sendRequestList(PROJECTS_URL, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
    }

}
