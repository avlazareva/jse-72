package ru.t1.lazareva.tm.unit.controller;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.lazareva.tm.api.service.IProjectDtoService;
import ru.t1.lazareva.tm.config.ApplicationConfiguration;
import ru.t1.lazareva.tm.config.WebApplicationConfiguration;
import ru.t1.lazareva.tm.dto.ProjectDTO;
import ru.t1.lazareva.tm.marker.UnitCategory;
import ru.t1.lazareva.tm.util.UserUtil;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class, ApplicationConfiguration.class})
@Category(UnitCategory.class)
public class ProjectControllerTest {

    @NotNull
    private static final String PROJECTS_URL = "http://localhost:8080/projects";
    @NotNull
    private static final String PROJECT_URL = "http://localhost:8080/project";
    @NotNull
    private final ProjectDTO project1 = new ProjectDTO("Test Project 1");
    @NotNull
    private final ProjectDTO project2 = new ProjectDTO("Test Project 2");
    @NotNull
    private final ProjectDTO project3 = new ProjectDTO("Test Project 3");
    @NotNull
    private final ProjectDTO project4 = new ProjectDTO("Test Project 4");
    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;
    @NotNull
    private MockMvc mockMvc;
    @NotNull
    @Autowired
    private WebApplicationContext wac;
    @NotNull
    @Autowired
    private IProjectDtoService projectService;

    @Before
    public void initTest() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("admin", "admin");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        project1.setUserId(UserUtil.getUserId());
        project2.setUserId(UserUtil.getUserId());
        project3.setUserId(UserUtil.getUserId());
        project4.setUserId(UserUtil.getUserId());
        projectService.save(UserUtil.getUserId(), project1);
        projectService.save(UserUtil.getUserId(), project2);
    }

    @After
    public void clear() {
        projectService.removeAll(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    public void findAllTest() {
        mockMvc.perform(MockMvcRequestBuilders.get(PROJECTS_URL))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void createTest() {
        Assert.assertEquals(2, projectService.findAll(UserUtil.getUserId()).size());
        mockMvc.perform(MockMvcRequestBuilders.get(PROJECT_URL + "/create"))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        Assert.assertEquals(3, projectService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void deleteTest() {
        Assert.assertEquals(2, projectService.findAll(UserUtil.getUserId()).size());
        mockMvc.perform(MockMvcRequestBuilders.get(PROJECT_URL + "/delete/" + project1.getId()))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        Assert.assertEquals(1, projectService.findAll(UserUtil.getUserId()).size());
        Assert.assertNull(
                projectService.findAll(UserUtil.getUserId()).stream()
                        .filter(m -> project1.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
    }

    @Test
    @SneakyThrows
    public void editTest() {
        mockMvc.perform(MockMvcRequestBuilders.get(PROJECT_URL + "/edit/" + project1.getId()))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
    }

}