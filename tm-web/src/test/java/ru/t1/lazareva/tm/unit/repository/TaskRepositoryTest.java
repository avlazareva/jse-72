package ru.t1.lazareva.tm.unit.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.lazareva.tm.config.ApplicationConfiguration;
import ru.t1.lazareva.tm.dto.TaskDTO;
import ru.t1.lazareva.tm.marker.UnitCategory;
import ru.t1.lazareva.tm.repository.ITaskDtoRepository;
import ru.t1.lazareva.tm.util.UserUtil;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
@Category(UnitCategory.class)
public class TaskRepositoryTest {

    @NotNull
    private final TaskDTO task1 = new TaskDTO("Test Task 1");

    @NotNull
    private final TaskDTO task2 = new TaskDTO("Test Task 2");

    @NotNull
    private final TaskDTO task3 = new TaskDTO("Test Task 3");

    @NotNull
    @Autowired
    private ITaskDtoRepository taskDtoRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void initTest() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("admin", "admin");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        task1.setUserId(UserUtil.getUserId());
        task2.setUserId(UserUtil.getUserId());
        task3.setUserId(UserUtil.getUserId());
        taskDtoRepository.save(task1);
        taskDtoRepository.save(task2);
    }

    @After
    public void clear() {
        taskDtoRepository.deleteAllByUserId(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    public void findAllByUserIdTest() {
        Assert.assertEquals(2, taskDtoRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void findByIdAndUserIdTest() {
        Assert.assertEquals(task1.getId(), taskDtoRepository.findByIdAndUserId(task1.getId(), UserUtil.getUserId()).getId());
    }

    @Test
    @SneakyThrows
    public void saveOneTest() {
        Assert.assertEquals(2, taskDtoRepository.findAllByUserId(UserUtil.getUserId()).size());
        taskDtoRepository.save(task3);
        Assert.assertEquals(3, taskDtoRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void deleteAllByUserIdTest() {
        Assert.assertEquals(2, taskDtoRepository.findAllByUserId(UserUtil.getUserId()).size());
        taskDtoRepository.deleteAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, taskDtoRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void deleteByIdAndUserIdTest() {
        Assert.assertEquals(2, taskDtoRepository.findAllByUserId(UserUtil.getUserId()).size());
        taskDtoRepository.deleteByIdAndUserId(task1.getId(), UserUtil.getUserId());
        Assert.assertEquals(1, taskDtoRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

}