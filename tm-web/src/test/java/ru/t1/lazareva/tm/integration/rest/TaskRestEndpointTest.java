package ru.t1.lazareva.tm.integration.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.t1.lazareva.tm.dto.CredentialsDto;
import ru.t1.lazareva.tm.dto.Result;
import ru.t1.lazareva.tm.dto.TaskDTO;
import ru.t1.lazareva.tm.marker.IntegrationCategory;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.List;

@Category(IntegrationCategory.class)
public class TaskRestEndpointTest {

    @NotNull
    private static final String TASK_URL = "http://localhost:8080/api/task";
    @NotNull
    private static final String TASKS_URL = "http://localhost:8080/api/tasks";
    @NotNull
    private static final HttpHeaders HEADER = new HttpHeaders();
    @Nullable
    private static String sessionId;
    @NotNull
    private final TaskDTO task1 = new TaskDTO("Test Task 1");
    @NotNull
    private final TaskDTO task2 = new TaskDTO("Test Task 2");
    @NotNull
    private final TaskDTO task3 = new TaskDTO("Test Task 3");

    @BeforeClass
    public static void beforeClass() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/api/auth/login";
        @NotNull CredentialsDto credentials = new CredentialsDto("admin", "admin");
        @NotNull final ResponseEntity<Result> response = restTemplate.postForEntity(url, new HttpEntity<CredentialsDto>(credentials, HEADER), Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        HttpHeaders headersResponse = response.getHeaders();
        List<HttpCookie> cookies = HttpCookie.parse(headersResponse.getFirst(HttpHeaders.SET_COOKIE));
        sessionId = cookies.stream()
                .filter(item -> "JSESSIONID".equals(item.getName()))
                .findFirst()
                .get()
                .getValue();
        Assert.assertNotNull(sessionId);
        HEADER.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
        HEADER.setContentType(MediaType.APPLICATION_JSON);
    }

    private static ResponseEntity<List> sendRequestList(@NotNull final String url, HttpMethod method, @NotNull final HttpEntity<List> httpEntity) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, List.class);
    }

    private static ResponseEntity<TaskDTO> sendRequest(@NotNull final String url, HttpMethod method, @NotNull final HttpEntity<TaskDTO> httpEntity) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, TaskDTO.class);
    }

    @AfterClass
    public static void logout() {
        @NotNull final String url = "http://localhost:8080/api/auth/logout";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(HEADER));
    }

    @Before
    public void init() {
        sendRequestList(TASKS_URL, HttpMethod.POST, new HttpEntity<>(Arrays.asList(task1, task2), HEADER));
    }

    @After
    public void clear() {
        sendRequestList(TASKS_URL, HttpMethod.DELETE, new HttpEntity<>(sendRequestList(TASKS_URL, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody(), HEADER));
    }

    @Test
    public void getTest() {
        Assert.assertEquals(task1.getName(), sendRequest(TASK_URL + "/" + task1.getId(), HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().getName());
    }

    @Test
    public void postTest() {
        Assert.assertEquals(2, sendRequestList(TASKS_URL, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
        sendRequest(TASK_URL, HttpMethod.POST, new HttpEntity<>(task3, HEADER));
        Assert.assertEquals(3, sendRequestList(TASKS_URL, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
    }

    @Test
    public void putTest() {
        Assert.assertEquals(2, sendRequestList(TASKS_URL, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
        task1.setName("Name 1");
        sendRequest(TASK_URL, HttpMethod.POST, new HttpEntity<>(task1, HEADER));
        Assert.assertEquals(2, sendRequestList(TASKS_URL, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
        Assert.assertEquals("Name 1", sendRequest(TASK_URL + "/" + task1.getId(), HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().getName());
    }

    @Test
    public void deleteTest() {
        Assert.assertEquals(2, sendRequestList(TASKS_URL, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
        sendRequest(TASK_URL + "/" + task1.getId(), HttpMethod.DELETE, new HttpEntity<>(task1, HEADER));
        Assert.assertEquals(1, sendRequestList(TASKS_URL, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
    }

}
