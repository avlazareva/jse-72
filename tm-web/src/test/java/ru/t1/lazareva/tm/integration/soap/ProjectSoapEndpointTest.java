package ru.t1.lazareva.tm.integration.soap;

import lombok.SneakyThrows;
import org.apache.cxf.helpers.CastUtils;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.springframework.http.HttpHeaders;
import ru.t1.lazareva.tm.api.endpoint.IAuthEndpoint;
import ru.t1.lazareva.tm.api.endpoint.IProjectEndpoint;
import ru.t1.lazareva.tm.api.endpoint.IProjectsEndpoint;
import ru.t1.lazareva.tm.client.AuthSoapEndpointClient;
import ru.t1.lazareva.tm.client.ProjectSoapEndpointClient;
import ru.t1.lazareva.tm.client.ProjectsSoapEndpointClient;
import ru.t1.lazareva.tm.dto.CredentialsDto;
import ru.t1.lazareva.tm.dto.ProjectDTO;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.util.*;

public class ProjectSoapEndpointTest {

    @NotNull
    private static final String URL = "http://localhost:8080";
    @NotNull
    private static IAuthEndpoint authEndpoint;
    @NotNull
    private static IProjectsEndpoint projectsEndpoint;
    @NotNull
    private static IProjectEndpoint projectEndpoint;
    @NotNull
    private static String userId;
    @NotNull
    private final ProjectDTO project1 = new ProjectDTO("Test Project 1");

    @NotNull
    private final ProjectDTO project2 = new ProjectDTO("Test Project 2");

    @NotNull
    private final ProjectDTO project3 = new ProjectDTO("Test Project 3");

    @NotNull
    private final ProjectDTO project4 = new ProjectDTO("Test Project 4");

    @BeforeClass
    @SneakyThrows
    public static void beforeClass() {
        authEndpoint = AuthSoapEndpointClient.getInstance(URL);
        Assert.assertTrue(authEndpoint.login(new CredentialsDto("admin", "admin")).getSuccess());
        projectEndpoint = ProjectSoapEndpointClient.getInstance(URL);
        projectsEndpoint = ProjectsSoapEndpointClient.getInstance(URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider projectBindingProvider = (BindingProvider) projectEndpoint;
        @NotNull final BindingProvider projectCollectionBindingProvider = (BindingProvider) projectsEndpoint;
        Map<String, List<String>> headers = CastUtils.cast((Map) authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS));
        if (headers == null) new HashMap<String, List<String>>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        projectBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
        projectCollectionBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    }

    @Before
    public void init() {
        projectsEndpoint.saveAll(Arrays.asList(project1, project2));
    }

    @After
    public void clear() {
        projectsEndpoint.deleteAll(projectsEndpoint.findAll());
    }

    @Test
    public void findOneByIdTest() {
        Assert.assertEquals(project1.getName(), projectEndpoint.findOneById(project1.getId()).getName());
    }

    @Test
    public void saveOneTest() {
        Assert.assertEquals(2, projectsEndpoint.findAll().size());
        projectEndpoint.saveOne(project3);
        Assert.assertEquals(3, projectsEndpoint.findAll().size());
    }

    @Test
    public void updateOneTest() {
        Assert.assertNotEquals("New name 1", projectEndpoint.findOneById(project1.getId()).getName());
        project1.setName("Name 1");
        projectEndpoint.updateOne(project1);
        Assert.assertEquals("Name 1", projectEndpoint.findOneById(project1.getId()).getName());
    }

    @Test
    public void deleteOneByIdTest() {
        Assert.assertEquals(2, projectsEndpoint.findAll().size());
        projectEndpoint.deleteOneById(project1.getId());
        Assert.assertEquals(1, projectsEndpoint.findAll().size());
    }

}