package ru.t1.lazareva.tm.unit.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.lazareva.tm.api.service.ITaskDtoService;
import ru.t1.lazareva.tm.config.ApplicationConfiguration;
import ru.t1.lazareva.tm.dto.TaskDTO;
import ru.t1.lazareva.tm.exception.EntityNotFoundException;
import ru.t1.lazareva.tm.exception.IdEmptyException;
import ru.t1.lazareva.tm.exception.UserIdEmptyException;
import ru.t1.lazareva.tm.marker.UnitCategory;
import ru.t1.lazareva.tm.util.UserUtil;

import java.util.Arrays;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
@Category(UnitCategory.class)
public class TaskServiceTest {

    @NotNull
    private final TaskDTO task1 = new TaskDTO("Test Task 1");
    @NotNull
    private final TaskDTO task2 = new TaskDTO("Test Task 2");
    @NotNull
    private final TaskDTO task3 = new TaskDTO("Test Task 3");
    @NotNull
    private final TaskDTO task4 = new TaskDTO("Test Task 4");
    @NotNull
    @Autowired
    private ITaskDtoService taskDtoService;
    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void initTest() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("admin", "admin");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        task1.setUserId(UserUtil.getUserId());
        task2.setUserId(UserUtil.getUserId());
        task3.setUserId(UserUtil.getUserId());
        taskDtoService.save(UserUtil.getUserId(), task1);
        taskDtoService.save(UserUtil.getUserId(), task2);
    }

    @After
    public void clear() {
        taskDtoService.removeAll(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    public void saveTest() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.save(null, task1));
        Assert.assertThrows(EntityNotFoundException.class, () -> taskDtoService.save(UserUtil.getUserId(), null));
        Assert.assertEquals(2, taskDtoService.findAll(UserUtil.getUserId()).size());
        taskDtoService.save(UserUtil.getUserId(), task3);
        Assert.assertEquals(3, taskDtoService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void saveAllTest() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.saveAll(null, Arrays.asList(task1)));
        Assert.assertThrows(EntityNotFoundException.class, () -> taskDtoService.saveAll(UserUtil.getUserId(), null));
        Assert.assertEquals(2, taskDtoService.findAll(UserUtil.getUserId()).size());
        taskDtoService.saveAll(UserUtil.getUserId(), Arrays.asList(task3, task4));
        Assert.assertEquals(4, taskDtoService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void removeAllByUserIdTest() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.removeAll(null));
        Assert.assertEquals(2, taskDtoService.findAll(UserUtil.getUserId()).size());
        taskDtoService.removeAll(UserUtil.getUserId());
        Assert.assertEquals(0, taskDtoService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void removeAllTest() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.removeAll(null));
        Assert.assertEquals(2, taskDtoService.findAll(UserUtil.getUserId()).size());
        taskDtoService.removeAll(UserUtil.getUserId());
        Assert.assertEquals(0, taskDtoService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void removeOneByIdTest() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.removeOneById(null, task1.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> taskDtoService.removeOneById(UserUtil.getUserId(), null));
        Assert.assertThrows(IdEmptyException.class, () -> taskDtoService.removeOneById(UserUtil.getUserId(), ""));
        Assert.assertEquals(2, taskDtoService.findAll(UserUtil.getUserId()).size());
        taskDtoService.removeOneById(UserUtil.getUserId(), task1.getId());
        Assert.assertEquals(1, taskDtoService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void findAllTest() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.findAll(null));
        Assert.assertEquals(2, taskDtoService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void findOneByIdTestNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.findOneById(null, task1.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> taskDtoService.findOneById(UserUtil.getUserId(), null));
        Assert.assertThrows(IdEmptyException.class, () -> taskDtoService.findOneById(UserUtil.getUserId(), ""));
        Assert.assertEquals(task1.getId(), taskDtoService.findOneById(UserUtil.getUserId(), task1.getId()).getId());
    }

}
