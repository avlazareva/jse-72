package ru.t1.lazareva.tm.unit.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.lazareva.tm.config.ApplicationConfiguration;
import ru.t1.lazareva.tm.dto.ProjectDTO;
import ru.t1.lazareva.tm.marker.UnitCategory;
import ru.t1.lazareva.tm.repository.IProjectDtoRepository;
import ru.t1.lazareva.tm.util.UserUtil;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
@Category(UnitCategory.class)
public class ProjectRepositoryTest {

    @NotNull
    private final ProjectDTO project1 = new ProjectDTO("Test Project 1");

    @NotNull
    private final ProjectDTO project2 = new ProjectDTO("Test Project 2");

    @NotNull
    private final ProjectDTO project3 = new ProjectDTO("Test Project 3");

    @NotNull
    @Autowired
    private IProjectDtoRepository projectDtoRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void initTest() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("admin", "admin");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        project1.setUserId(UserUtil.getUserId());
        project2.setUserId(UserUtil.getUserId());
        project3.setUserId(UserUtil.getUserId());
        projectDtoRepository.save(project1);
        projectDtoRepository.save(project2);
    }

    @After
    public void clear() {
        projectDtoRepository.deleteAllByUserId(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    public void findAllByUserIdTest() {
        Assert.assertEquals(2, projectDtoRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void findByIdAndUserIdTest() {
        Assert.assertEquals(project1.getId(), projectDtoRepository.findByIdAndUserId(project1.getId(), UserUtil.getUserId()).getId());
    }

    @Test
    @SneakyThrows
    public void saveOneTest() {
        Assert.assertEquals(2, projectDtoRepository.findAllByUserId(UserUtil.getUserId()).size());
        projectDtoRepository.save(project3);
        Assert.assertEquals(3, projectDtoRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void deleteAllByUserIdTest() {
        Assert.assertEquals(2, projectDtoRepository.findAllByUserId(UserUtil.getUserId()).size());
        projectDtoRepository.deleteAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, projectDtoRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void deleteByIdAndUserIdTest() {
        Assert.assertEquals(2, projectDtoRepository.findAllByUserId(UserUtil.getUserId()).size());
        projectDtoRepository.deleteByIdAndUserId(project1.getId(), UserUtil.getUserId());
        Assert.assertEquals(1, projectDtoRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

}
