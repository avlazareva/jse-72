package ru.t1.lazareva.tm.integration.soap;

import lombok.SneakyThrows;
import org.apache.cxf.helpers.CastUtils;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.springframework.http.HttpHeaders;
import ru.t1.lazareva.tm.api.endpoint.IAuthEndpoint;
import ru.t1.lazareva.tm.api.endpoint.ITaskEndpoint;
import ru.t1.lazareva.tm.api.endpoint.ITasksEndpoint;
import ru.t1.lazareva.tm.client.AuthSoapEndpointClient;
import ru.t1.lazareva.tm.client.TaskSoapEndpointClient;
import ru.t1.lazareva.tm.client.TasksSoapEndpointClient;
import ru.t1.lazareva.tm.dto.CredentialsDto;
import ru.t1.lazareva.tm.dto.TaskDTO;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.util.*;

public class TasksSoapEndpointTest {

    @NotNull
    private static final String URL = "http://localhost:8080";
    @NotNull
    private static IAuthEndpoint authEndpoint;
    @NotNull
    private static ITasksEndpoint tasksEndpoint;
    @NotNull
    private static ITaskEndpoint taskEndpoint;
    @NotNull
    private static String userId;
    @NotNull
    private final TaskDTO task1 = new TaskDTO("Test Task 1");

    @NotNull
    private final TaskDTO task2 = new TaskDTO("Test Task 2");

    @NotNull
    private final TaskDTO task3 = new TaskDTO("Test Task 3");

    @NotNull
    private final TaskDTO task4 = new TaskDTO("Test Task 4");

    @BeforeClass
    @SneakyThrows
    public static void beforeClass() {
        authEndpoint = AuthSoapEndpointClient.getInstance(URL);
        Assert.assertTrue(authEndpoint.login(new CredentialsDto("admin", "admin")).getSuccess());
        taskEndpoint = TaskSoapEndpointClient.getInstance(URL);
        tasksEndpoint = TasksSoapEndpointClient.getInstance(URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider taskBindingProvider = (BindingProvider) taskEndpoint;
        @NotNull final BindingProvider taskCollectionBindingProvider = (BindingProvider) tasksEndpoint;
        Map<String, List<String>> headers = CastUtils.cast((Map) authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS));
        if (headers == null) new HashMap<String, List<String>>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        taskBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
        taskCollectionBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    }

    @Before
    public void init() {
        tasksEndpoint.saveAll(Arrays.asList(task1, task2));
    }

    @After
    public void clear() {
        tasksEndpoint.deleteAll();
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(2, tasksEndpoint.findAll().size());
        @NotNull List<TaskDTO> tasks = Arrays.asList(task1, task2);
        for (final TaskDTO task : tasks) {
            Assert.assertNotNull(
                    tasksEndpoint
                            .findAll()
                            .stream()
                            .filter(m -> task.getId().equals(m.getId()))
                            .findFirst()
                            .orElse(null)
            );
        }
    }

    @Test
    public void saveCollectionTest() {
        Assert.assertEquals(2, tasksEndpoint.findAll().size());
        tasksEndpoint.saveAll(Arrays.asList(task3, task4));
        Assert.assertEquals(4, tasksEndpoint.findAll().size());
    }

    @Test
    public void updateCollectionTest() {
        Assert.assertEquals(2, tasksEndpoint.findAll().size());
        Assert.assertNotEquals("Name 1", tasksEndpoint.findAll().get(0).getName());
        Assert.assertNotEquals("Name 2", tasksEndpoint.findAll().get(1).getName());
        task1.setName("Name 1");
        task2.setName("Name 2");
        tasksEndpoint.updateAll(Arrays.asList(task1, task2));
        Assert.assertEquals(2, tasksEndpoint.findAll().size());
        Assert.assertEquals("Name 1", tasksEndpoint.findAll().get(0).getName());
        Assert.assertEquals("Name 2", tasksEndpoint.findAll().get(1).getName());
    }

    @Test
    public void deleteCollectionTest() {
        Assert.assertNotNull(tasksEndpoint.findAll());
        tasksEndpoint.deleteAll();
        Assert.assertNull(tasksEndpoint.findAll());
    }

}
