package ru.t1.lazareva.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.lazareva.tm.config.ApplicationConfiguration;
import ru.t1.lazareva.tm.config.WebApplicationConfiguration;
import ru.t1.lazareva.tm.dto.TaskDTO;
import ru.t1.lazareva.tm.marker.UnitCategory;
import ru.t1.lazareva.tm.util.UserUtil;

import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class, ApplicationConfiguration.class})
@Category(UnitCategory.class)
public class TaskEndpointTest {

    @NotNull
    private static final String PROJECT_URL = "http://localhost:8080/api/task";
    @NotNull
    private static final String PROJECTS_URL = "http://localhost:8080/api/tasks";
    @NotNull
    private final TaskDTO task1 = new TaskDTO("Test Task 1");
    @NotNull
    private final TaskDTO task2 = new TaskDTO("Test Task 2");
    @NotNull
    private final TaskDTO task3 = new TaskDTO("Test Task 3");
    @NotNull
    private final TaskDTO task4 = new TaskDTO("Test Task 4");
    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;
    @NotNull
    private MockMvc mockMvc;
    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @Before
    public void initTest() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("admin", "admin");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        task1.setUserId(UserUtil.getUserId());
        task2.setUserId(UserUtil.getUserId());
        task3.setUserId(UserUtil.getUserId());
        task4.setUserId(UserUtil.getUserId());
        taskPost(task1);
        taskPost(task2);
    }

    @After
    public void clear() {
        tasksDelete(tasksGet());
    }

    @SneakyThrows
    private TaskDTO taskGet(@NotNull final String id) {
        @NotNull final String url = PROJECT_URL + "/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if ("".equals(json)) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, TaskDTO.class);
    }

    @SneakyThrows
    private List<TaskDTO> tasksGet() {
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(PROJECTS_URL)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if ("".equals(json)) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return Arrays.asList(objectMapper.readValue(json, TaskDTO[].class));
    }


    @Test
    @SneakyThrows
    public void taskGetTest() {
        Assert.assertEquals(task1.getName(), taskGet(task1.getId()).getName());
    }

    @Test
    @SneakyThrows
    public void tasksGetTest() {
        Assert.assertEquals(2, tasksGet().size());
        Assert.assertNotNull(
                tasksGet().stream()
                        .filter(m -> task1.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
        Assert.assertNotNull(
                tasksGet().stream()
                        .filter(m -> task2.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
    }

    @SneakyThrows
    private void taskPost(@NotNull final TaskDTO task) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(task);
        mockMvc.perform(MockMvcRequestBuilders.post(PROJECT_URL).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void tasksPost(@NotNull final List<TaskDTO> tasks) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(tasks);
        mockMvc.perform(MockMvcRequestBuilders.post(PROJECTS_URL).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void taskPostTest() {
        Assert.assertEquals(2, tasksGet().size());
        Assert.assertNull(
                tasksGet().stream()
                        .filter(m -> task3.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
        taskPost(task3);
        Assert.assertEquals(3, tasksGet().size());
        Assert.assertNotNull(
                tasksGet().stream()
                        .filter(m -> task3.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
    }

    @Test
    @SneakyThrows
    public void tasksPostTest() {
        Assert.assertEquals(2, tasksGet().size());
        tasksPost(Arrays.asList(task3, task4));
        Assert.assertEquals(4, tasksGet().size());
        Assert.assertNotNull(
                tasksGet().stream()
                        .filter(m -> task3.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
        Assert.assertNotNull(
                tasksGet().stream()
                        .filter(m -> task4.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
    }

    @SneakyThrows
    private void taskPut(@NotNull final TaskDTO task) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(task);
        mockMvc.perform(MockMvcRequestBuilders.post(PROJECT_URL).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void tasksPut(@NotNull final List<TaskDTO> tasks) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(tasks);
        mockMvc.perform(MockMvcRequestBuilders.post(PROJECTS_URL).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void taskPutTest() {
        Assert.assertNotEquals("Name for task 1", taskGet(task1.getId()).getName());
        Assert.assertNull(
                tasksGet().stream()
                        .filter(m -> task3.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
        task1.setName("Name for task 1");
        taskPut(task1);
        Assert.assertEquals("Name for task 1", taskGet(task1.getId()).getName());
    }

    @Test
    @SneakyThrows
    public void tasksPutTest() {
        Assert.assertNotEquals("Name for task 1", taskGet(task1.getId()).getName());
        Assert.assertNotEquals("Name for task 2", taskGet(task2.getId()).getName());
        task1.setName("Name for task 1");
        task2.setName("Name for task 2");
        tasksPost(Arrays.asList(task1, task2));
        Assert.assertEquals("Name for task 1", taskGet(task1.getId()).getName());
        Assert.assertEquals("Name for task 2", taskGet(task2.getId()).getName());
    }

    @SneakyThrows
    private void taskDelete(@NotNull final String id) {
        @NotNull final String url = PROJECT_URL + "/" + id;
        mockMvc.perform(MockMvcRequestBuilders.delete(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void tasksDelete(@NotNull final List<TaskDTO> tasks) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(tasks);
        mockMvc.perform(MockMvcRequestBuilders.delete(PROJECTS_URL).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void taskDeleteTest() {
        Assert.assertEquals(2, tasksGet().size());
        Assert.assertNotNull(
                tasksGet().stream()
                        .filter(m -> task1.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
        taskDelete(task1.getId());
        Assert.assertEquals(1, tasksGet().size());
        Assert.assertNull(
                tasksGet().stream()
                        .filter(m -> task1.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
    }

    @Test
    @SneakyThrows
    public void tasksDeleteTest() {
        Assert.assertEquals(2, tasksGet().size());
        tasksDelete(Arrays.asList(task1, task2));
        Assert.assertEquals(0, tasksGet().size());
    }

}