package ru.t1.lazareva.tm.integration.soap;

import lombok.SneakyThrows;
import org.apache.cxf.helpers.CastUtils;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.springframework.http.HttpHeaders;
import ru.t1.lazareva.tm.api.endpoint.IAuthEndpoint;
import ru.t1.lazareva.tm.api.endpoint.ITaskEndpoint;
import ru.t1.lazareva.tm.api.endpoint.ITasksEndpoint;
import ru.t1.lazareva.tm.client.AuthSoapEndpointClient;
import ru.t1.lazareva.tm.client.TaskSoapEndpointClient;
import ru.t1.lazareva.tm.client.TasksSoapEndpointClient;
import ru.t1.lazareva.tm.dto.CredentialsDto;
import ru.t1.lazareva.tm.dto.TaskDTO;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.util.*;

public class TaskSoapEndpointTest {

    @NotNull
    private static final String URL = "http://localhost:8080";
    @NotNull
    private static IAuthEndpoint authEndpoint;
    @NotNull
    private static ITasksEndpoint tasksEndpoint;
    @NotNull
    private static ITaskEndpoint taskEndpoint;
    @NotNull
    private static String userId;
    @NotNull
    private final TaskDTO task1 = new TaskDTO("Test Task 1");

    @NotNull
    private final TaskDTO task2 = new TaskDTO("Test Task 2");

    @NotNull
    private final TaskDTO task3 = new TaskDTO("Test Task 3");

    @NotNull
    private final TaskDTO task4 = new TaskDTO("Test Task 4");

    @BeforeClass
    @SneakyThrows
    public static void beforeClass() {
        authEndpoint = AuthSoapEndpointClient.getInstance(URL);
        Assert.assertTrue(authEndpoint.login(new CredentialsDto("admin", "admin")).getSuccess());
        taskEndpoint = TaskSoapEndpointClient.getInstance(URL);
        tasksEndpoint = TasksSoapEndpointClient.getInstance(URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider taskBindingProvider = (BindingProvider) taskEndpoint;
        @NotNull final BindingProvider taskCollectionBindingProvider = (BindingProvider) tasksEndpoint;
        Map<String, List<String>> headers = CastUtils.cast((Map) authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS));
        if (headers == null) new HashMap<String, List<String>>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        taskBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
        taskCollectionBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    }

    @Before
    public void init() {
        tasksEndpoint.saveAll(Arrays.asList(task1, task2));
    }

    @After
    public void clear() {
        tasksEndpoint.deleteAll();
    }

    @Test
    public void findOneByIdTest() {
        Assert.assertEquals(task1.getName(), taskEndpoint.findOneById(task1.getId()).getName());
    }

    @Test
    public void saveOneTest() {
        Assert.assertEquals(2, tasksEndpoint.findAll().size());
        taskEndpoint.saveOne(task3);
        Assert.assertEquals(3, tasksEndpoint.findAll().size());
    }

    @Test
    public void updateOneTest() {
        Assert.assertNotEquals("Name 1", taskEndpoint.findOneById(task1.getId()).getName());
        task1.setName("Name 1");
        taskEndpoint.updateOne(task1);
        Assert.assertEquals("Name 1", taskEndpoint.findOneById(task1.getId()).getName());
    }

    @Test
    public void deleteOneByIdTest() {
        Assert.assertEquals(2, tasksEndpoint.findAll().size());
        taskEndpoint.deleteOneById(task1.getId());
        Assert.assertEquals(1, tasksEndpoint.findAll().size());
    }

}
