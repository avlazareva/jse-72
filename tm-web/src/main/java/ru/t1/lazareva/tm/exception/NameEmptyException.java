package ru.t1.lazareva.tm.exception;

public final class NameEmptyException extends AbstractException {

    public NameEmptyException() {
        super("Error! Name is empty...");
    }

}