package ru.t1.lazareva.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.api.endpoint.IProjectsEndpoint;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import java.net.URL;

public class ProjectsSoapEndpointClient {

    @SneakyThrows
    public static IProjectsEndpoint getInstance(@NotNull final String baseURL) {
        @NotNull final String wsdl = baseURL + "/jaxws/ProjectsEndpointWS?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final String lp = "ProjectsEndpointImplService";
        @NotNull final String ns = "http://endpoint.tm.lazareva.t1.ru/";
        @NotNull final QName name = new QName(ns, lp);
        @NotNull final IProjectsEndpoint result = Service.create(url, name).getPort(IProjectsEndpoint.class);
        @NotNull final BindingProvider bindingProvider = (BindingProvider) result;
        bindingProvider.getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
        return result;
    }

}
