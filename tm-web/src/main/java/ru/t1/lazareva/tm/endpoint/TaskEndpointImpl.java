package ru.t1.lazareva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.t1.lazareva.tm.api.endpoint.ITaskEndpoint;
import ru.t1.lazareva.tm.api.service.ITaskDtoService;
import ru.t1.lazareva.tm.dto.TaskDTO;
import ru.t1.lazareva.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@RestController
@RequestMapping("/api/task")
@WebService(endpointInterface = "ru.t1.lazareva.tm.api.endpoint.ITaskEndpoint", serviceName = "TaskEndpointImplService", name = "TaskEndpointImplService")
public class TaskEndpointImpl implements ITaskEndpoint {

    @NotNull
    @Autowired
    private ITaskDtoService taskService;

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public TaskDTO findOneById(@WebParam(name = "id", partName = "id") @NotNull @PathVariable("id") String id) {
        return taskService.findOneById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PostMapping
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void saveOne(@WebParam(name = "task", partName = "task") @NotNull @RequestBody TaskDTO task) {
        taskService.save(UserUtil.getUserId(), task);
    }

    @Override
    @WebMethod
    @PutMapping
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void updateOne(@WebParam(name = "task", partName = "task") @NotNull @RequestBody TaskDTO task) {
        taskService.save(UserUtil.getUserId(), task);
    }

    @Override
    @WebMethod
    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void deleteOneById(@WebParam(name = "id", partName = "id") @NotNull @PathVariable("id") String id) {
        taskService.removeOneById(UserUtil.getUserId(), id);
    }

}