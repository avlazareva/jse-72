package ru.t1.lazareva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.lazareva.tm.api.service.ITaskDtoService;
import ru.t1.lazareva.tm.dto.soap.*;
import ru.t1.lazareva.tm.util.UserUtil;

@Endpoint
public class TasksSoapEndpoint {

    @NotNull
    public final static String LOCATION_URI = "/ws";

    @NotNull
    public final static String PORT_TYPE_NAME = "TaskCollectionSoapEndpointPort";

    @NotNull
    public final static String NAMESPACE = "http://tm.lazareva.t1.ru/dto/soap";

    @NotNull
    @Autowired
    private ITaskDtoService taskService;

    @Nullable
    @ResponsePayload
    @PayloadRoot(localPart = "tasksFindAllRequest", namespace = NAMESPACE)
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public TasksFindAllResponse tasksFindAll() {
        return new TasksFindAllResponse(taskService.findAll(UserUtil.getUserId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "tasksSaveRequest", namespace = NAMESPACE)
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public TasksSaveResponse tasksSave(@RequestPayload final TasksSaveRequest request) {
        taskService.saveAll(UserUtil.getUserId(), request.getTasks());
        return new TasksSaveResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "tasksUpdateRequest", namespace = NAMESPACE)
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public TasksUpdateResponse tasksUpdate(@RequestPayload final TasksUpdateRequest request) {
        taskService.saveAll(UserUtil.getUserId(), request.getTasks());
        return new TasksUpdateResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "tasksDeleteRequest", namespace = NAMESPACE)
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public TasksDeleteResponse removeOne() {
        taskService.removeAll(UserUtil.getUserId());
        return new TasksDeleteResponse();
    }

}