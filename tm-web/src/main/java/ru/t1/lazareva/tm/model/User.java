package ru.t1.lazareva.tm.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "users")
public class User {

    @Id
    private String id = UUID.randomUUID().toString();

    @Column(name = "login")
    private String login;

    @Column(name = "password_hash")
    private String passwordHash;

    @OneToMany(mappedBy = "users", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Role> roles = new ArrayList<>();

    public User() {
    }

    public User(final String login, final String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

}
