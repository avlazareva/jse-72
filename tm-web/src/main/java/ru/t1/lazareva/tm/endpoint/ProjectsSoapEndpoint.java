package ru.t1.lazareva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.lazareva.tm.api.service.IProjectDtoService;
import ru.t1.lazareva.tm.dto.soap.*;
import ru.t1.lazareva.tm.util.UserUtil;

@Endpoint
public class ProjectsSoapEndpoint {

    @NotNull
    public final static String LOCATION_URI = "/ws";

    @NotNull
    public final static String PORT_TYPE_NAME = "ProjectsSoapEndpointPort";

    @NotNull
    public final static String NAMESPACE = "http://tm.lazareva.t1.ru/dto/soap";

    @NotNull
    @Autowired
    private IProjectDtoService projectService;

    @Nullable
    @ResponsePayload
    @PayloadRoot(localPart = "projectsFindAllRequest", namespace = NAMESPACE)
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public ProjectsFindAllResponse projectsFindAll() {
        return new ProjectsFindAllResponse(projectService.findAll(UserUtil.getUserId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectsSaveRequest", namespace = NAMESPACE)
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public ProjectsSaveResponse projectsSave(@RequestPayload final ProjectsSaveRequest request) {
        projectService.saveAll(UserUtil.getUserId(), request.getProjects());
        return new ProjectsSaveResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectsUpdateRequest", namespace = NAMESPACE)
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public ProjectsUpdateResponse projectsUpdate(@RequestPayload final ProjectsUpdateRequest request) {
        projectService.saveAll(UserUtil.getUserId(), request.getProjects());
        return new ProjectsUpdateResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectsDeleteRequest", namespace = NAMESPACE)
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public ProjectsDeleteResponse projectsDelete() {
        projectService.removeAll(UserUtil.getUserId());
        return new ProjectsDeleteResponse();
    }

}
