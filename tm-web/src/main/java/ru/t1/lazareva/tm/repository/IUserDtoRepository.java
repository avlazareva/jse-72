package ru.t1.lazareva.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.lazareva.tm.dto.UserDTO;

@Repository
public interface IUserDtoRepository extends JpaRepository<UserDTO, String> {

    UserDTO findByLogin(final String login);

}