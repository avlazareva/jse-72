package ru.t1.lazareva.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.t1.lazareva.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITasksEndpoint {

    @Nullable
    @GetMapping
    @WebMethod
    List<TaskDTO> findAll();

    @PostMapping
    @WebMethod
    void saveAll(
            @WebParam(name = "tasks", partName = "tasks")
            @NotNull @RequestBody List<TaskDTO> tasks);

    @PutMapping
    @WebMethod
    void updateAll(
            @WebParam(name = "tasks", partName = "tasks")
            @NotNull @RequestBody List<TaskDTO> tasks);

    @DeleteMapping
    @WebMethod
    void deleteAll();

}
