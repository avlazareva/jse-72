package ru.t1.lazareva.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.t1.lazareva.tm.dto.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectsEndpoint {

    @Nullable
    @GetMapping
    @WebMethod
    List<ProjectDTO> findAll();


    @PostMapping
    @WebMethod
    void saveAll(
            @WebParam(name = "projects", partName = "projects")
            @NotNull @RequestBody List<ProjectDTO> projects);

    @PutMapping
    @WebMethod
    void updateAll(
            @WebParam(name = "projects", partName = "projects")
            @NotNull @RequestBody List<ProjectDTO> projects);

    @DeleteMapping
    @WebMethod
    void deleteAll(
            @WebParam(name = "projects", partName = "projects")
            @NotNull @RequestBody List<ProjectDTO> projects);

}


