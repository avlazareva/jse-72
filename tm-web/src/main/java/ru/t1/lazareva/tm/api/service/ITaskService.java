package ru.t1.lazareva.tm.api.service;

import ru.t1.lazareva.tm.model.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    void save(final String userId, final Task task);

    void saveAll(final String userId, final Collection<Task> tasks);

    void removeAll(final String userId);

    void removeOneById(final String userId, final String id);

    List<Task> findAll(final String userId);

    Task findOneById(final String userId, final String id);

}
