package ru.t1.lazareva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.t1.lazareva.tm.api.endpoint.IProjectEndpoint;
import ru.t1.lazareva.tm.api.service.IProjectDtoService;
import ru.t1.lazareva.tm.dto.ProjectDTO;
import ru.t1.lazareva.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@RestController
@RequestMapping("/api/project")
@WebService(endpointInterface = "ru.t1.lazareva.tm.api.endpoint.IProjectEndpoint", serviceName = "ProjectEndpointImplService", name = "ProjectEndpointImplService")
public class ProjectEndpointImpl implements IProjectEndpoint {

    @NotNull
    @Autowired
    private IProjectDtoService projectService;

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public ProjectDTO findOneById(@WebParam(name = "id", partName = "id") @NotNull @PathVariable("id") String id) {
        return projectService.findOneById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PostMapping
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void saveOne(@WebParam(name = "project", partName = "project") @NotNull @RequestBody ProjectDTO project) {
        projectService.save(UserUtil.getUserId(), project);
    }

    @Override
    @WebMethod
    @PutMapping
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void updateOne(@WebParam(name = "project", partName = "project") @NotNull @RequestBody ProjectDTO project) {
        projectService.save(UserUtil.getUserId(), project);
    }

    @Override
    @WebMethod
    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void deleteOneById(@WebParam(name = "id", partName = "id") @NotNull @PathVariable("id") String id) {
        projectService.removeOneById(UserUtil.getUserId(), id);
    }

}