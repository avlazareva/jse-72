package ru.t1.lazareva.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.api.endpoint.ITasksEndpoint;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import java.net.URL;

public class TasksSoapEndpointClient {

    @SneakyThrows
    public static ITasksEndpoint getInstance(@NotNull final String baseURL) {
        @NotNull final String wsdl = baseURL + "/jaxws/TasksEndpointWS?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final String lp = "TasksEndpointImplService";
        @NotNull final String ns = "http://endpoint.tm.lazareva.t1.ru/";
        @NotNull final QName name = new QName(ns, lp);
        @NotNull final ITasksEndpoint result = Service.create(url, name).getPort(ITasksEndpoint.class);
        @NotNull final BindingProvider bindingProvider = (BindingProvider) result;
        bindingProvider.getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
        return result;
    }

}
