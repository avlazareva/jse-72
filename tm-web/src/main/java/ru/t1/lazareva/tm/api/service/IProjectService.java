package ru.t1.lazareva.tm.api.service;

import ru.t1.lazareva.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    void save(final String userId, final Project project);

    void saveAll(final String userId, final Collection<Project> projects);

    void removeAll(final String userId);

    void removeOneById(final String userId, final String id);

    List<Project> findAll(final String userId);

    Project findOneById(final String userId, final String id);

}