package ru.t1.lazareva.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.lazareva.tm.api.service.IProjectDtoService;
import ru.t1.lazareva.tm.api.service.ITaskDtoService;
import ru.t1.lazareva.tm.model.CustomUser;

@Controller
public class TasksController {

    @Autowired
    private ITaskDtoService taskService;

    @Autowired
    private IProjectDtoService projectService;

    @GetMapping("/tasks")
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public ModelAndView index(@AuthenticationPrincipal final CustomUser user) {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-list");
        modelAndView.addObject("tasks", taskService.findAll(user.getUserId()));
        modelAndView.addObject("projectService", projectService);
        return modelAndView;
    }

}
